---
title: "Cyberpunk 2077 Soundtrack: Punk’s not dead, it just chromed up"
date: 2021-05-14
affiliatelink: "https://www.soundspheremag.com/news/culture/cyberpunk-2077-soundtrack-punks-not-dead-it-just-chromed-up/"
imgurl: "post/Cyberpunk2077_Used_To_Live_Here_RGB_EN.png"
imgalt: "Johnny Silverhand leaning against the wall of the Afterlife"
tags: ["music", "Cyberpunk", "2077", "review"]
---

